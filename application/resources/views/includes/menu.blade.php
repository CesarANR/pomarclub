<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
 <div class="container-fluid">
  <div class="navbar-header">
    <button type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" class="navbar-toggle collapsed">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a href="#page-top" class="navbar-brand page-scroll">Pomar</a>
  </div>
  <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
   <ul class="nav navbar-nav navbar-right">
    <li><a href="#servico" class="page-scroll">Ao seu serviço</a></li>
    <li><a href="#produtos" class="page-scroll">Cestas</a></li>
    <li><a href="#feiras" class="page-scroll">Feiras</a></li>
    <li><a href="#eventos" class="page-scroll">Eventos</a></li>
    <li><a href="#quemsomos" class="page-scroll">Quem somos</a></li>
    <li><a href="#contato" class="page-scroll">Contato</a></li>
  </ul>
</div>
</div>
</nav>