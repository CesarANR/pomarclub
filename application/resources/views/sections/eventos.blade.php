 <div class="container">
    <div class="row">
       <div class="col-lg-8 col-lg-offset-2 text-center">
          <h2 class="section-heading">Eventos</h2>
          <hr>
          <div>
             <i style="vertical-align: middle;" data-wow-delay=".1s" class="fa fa-calendar fa-5x wow bounceIn"></i>
             <aside style="width: 300px; display: inline-block; text-align: left; vertical-align: middle; margin-left: 1.5em; font-size: 1.2em">
                <div>Condomínio Vivendas do Imbuí</div>
                <div style="color: #888">Sábado (27/02/2016) - 8 às 13h.</div>
                <div>Feira orgânica da pomar</div>
                <div>
                   <small>
                      <p>Parceiros: <em>Biofeira</em> e <em>Cosme e Damião</em></p>
                  </small>
              </div>
          </aside>
      </div>
      <div>
         <i style="vertical-align: middle;" data-wow-delay=".1s" class="fa fa-calendar fa-5x wow bounceIn"></i>
         <aside style="width: 300px; display: inline-block; text-align: left; vertical-align: middle; margin-left: 1.5em; font-size: 1.2em">
            <div>Condomínio Etco Greenville</div>
            <div style="color: #888">Sábado (20/02/2016) - 8 às 13h.</div>
            <div>Feira orgânica da pomar</div>
            <div>
               <small>
                  <p>Parceiros: <em>Biofeira</em> e <em>Cosme e Damião</em></p>
              </small>
          </div>
      </aside>
  </div>
  <div>
     <i style="vertical-align: middle;" data-wow-delay=".1s" class="fa fa-calendar fa-5x wow bounceIn"></i>
     <aside style="width: 300px; display: inline-block; text-align: left; vertical-align: middle; margin-left: 1.5em; font-size: 1.2em">
        <div>Condomínio Villa Tropical</div>
        <div style="color: #888">Sábado (30/01/2016) - 8 às 13h.</div>
        <div>Feira orgânica da pomar</div>
        <div>
           <small>
              <p>Parceiros: <em>Biofeira</em> e <em>Cosme e Damião</em></p>
          </small>
      </div>
  </aside>
</div>
</div>
</div>
</div>