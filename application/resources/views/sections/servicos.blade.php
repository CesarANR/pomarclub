 <div class="container">
    <div class="row">
       <div class="col-lg-12 text-center">
          <h2 class="section-heading">Ao seu serviço</h2>
          <hr class="primary">
      </div>
  </div>
</div>
<div class="container">
    <div class="row">
       <div class="col-lg-3 col-md-6 text-center">
          <div class="service-box">
             <img src="img/marketing8.png" class="wow bounceIn text-primary info-icon">
             <h3>Como funciona?</h3>
             <p class="text-muted">Você se associa à Pomar e toda semana receberá nossa cesta com a seleção dos melhores legumes, verduras, raízes e temperos da estação. Além de produtos orgânicos exclusivos dos nossos parceiros.</p>
         </div>
     </div>
     <div class="col-lg-3 col-md-6 text-center">
      <div class="service-box">
         <img src="img/vegan.png" class="wow bounceIn text-primary info-icon">
         <h3>Por que orgânicos?</h3>
         <p class="text-muted">Os benefícios de consumir orgânicos vão além da alimentação saudável e sabor! Te convidamos a aderir a um estilo de vida que apoia a agricultura sustentável e respeita o meio ambiente.</p>
     </div>
 </div>
 <div class="col-lg-3 col-md-6 text-center">
  <div class="service-box">
     <img src="img/transport.png" class="wow bounceIn text-primary info-icon">
     <h3>Por que a Pomar?</h3>
     <p class="text-muted">Nossos produtos vêm da horta direto para sua mesa, evitando passar por caminhões abarrotados, embalagens em excesso, estoques de supermercados e ficar por horas nas prateleiras.</p>
 </div>
</div>
<div class="col-lg-3 col-md-6 text-center">
  <div class="service-box">
     <img src="img/house.png" class="wow bounceIn text-primary info-icon">
     <h3>Condomínios</h3>
     <p class="text-muted">Além das nossas entregas em diversos bairros de Salvador, também promovemos feiras orgânicas nas áreas comuns dos condomínios.</p>
 </div>
</div>
</div>
</div>