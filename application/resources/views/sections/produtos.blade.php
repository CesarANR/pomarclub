 <div class="container">
    <div class="row">
       <div class="col-lg-12 text-center">
          <h2 class="section-heading cestas-header">Nossas cestas</h2>
          <hr class="light">
      </div>
  </div>
  <div class="row cestasRow flex-center-wrap">
   <div class="col-md-3 col-sm-6 col-xs-12 text-center service-box">
      <h2 class="section-heading"><a data-toggle="tooltip" data-html="true" data-placement="bottom" title="&lt;img src=&quot;img/cestas/essenciais.jpg&quot;&gt;" class="img-tooltip">Pomar Essenciais</a></h2>
      <h4>8 itens</h4>
      <p><span>2 folhas&nbsp;<i data-toggle="tooltip" data-placement="bottom" title="Alface americano, Alface crespa, Alface lisa, Alface roxa, Couve, Espinafre, Rúcula" style="cursor: pointer" class="fa fa-info-circle"></i></span><br><span>2 raízes&nbsp;<i data-toggle="tooltip" data-placement="bottom" title="Beterraba, Cenoura, Batata doce, Batata, Rabanete, Inhame, Aipim" style="cursor: pointer" class="fa fa-info-circle"></i></span><br><span>2 legumes&nbsp;<i data-toggle="tooltip" data-placement="bottom" title="Abobrinha, Berinjela, Brócolis, Pepino Comum, Tomate, Pimentão, Quiabo, Couve-flor" style="cursor: pointer" class="fa fa-info-circle"></i></span><br><span>1 tempero&nbsp;<i data-toggle="tooltip" data-placement="bottom" title="Alho poró, Salsa, Cebolinha, Coentro, Cebola, Cuminho em pó, Pimenta do Reino em pó, Coloral, Canela em pó, Canela em pó" style="cursor: pointer" class="fa fa-info-circle"></i></span><br><span>1 produto Cosme e Damião&nbsp;<i data-toggle="tooltip" data-placement="bottom" title="Polpa de cupuaçu, Cupuaçu em pó, Cacau em pó, Amêndoa de cacau, Banana passa, Açaí, Mel de cacau, Geleia de cacau, Doce de cupuaçu, Tabletes sortidos de cacau amargo, cupuaçu amargo e cacau com banana, Polpa de acerola, Polpa de graviola" style="cursor: pointer" class="fa fa-info-circle"></i></span><br></p>
      <div class="flex">
         <div class="bottom">
            <a target="_blank" href="explanation.pdf" class="linkSaibaMais">Saiba mais!</a>
            <h4>R$ 210 / mês *</h4>
            <form target="_blank" action="https://pagseguro.uol.com.br/v2/pre-approvals/request.html" method="post"><input type="hidden" name="code" value="FDB8736EA7A7887444899FB6422FF53F"><input type="image" src="https://stc.pagseguro.uol.com.br/public/img/botoes/pagamentos/205x30-comprar.gif" name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!"></form>
        </div>
    </div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 text-center service-box">
  <h2 class="section-heading"><a data-toggle="tooltip" data-html="true" data-placement="bottom" title="&lt;img src=&quot;img/cestas/banquete.jpg&quot;&gt;" class="img-tooltip">Pomar Banquete</a></h2>
  <h4>14 itens</h4>
  <p><span>4 folhas&nbsp;<i data-toggle="tooltip" data-placement="bottom" title="Alface americano, Alface crespa, Alface lisa, Alface roxa, Couve, Espinafre, Rúcula" style="cursor: pointer" class="fa fa-info-circle"></i></span><br><span>2 raízes&nbsp;<i data-toggle="tooltip" data-placement="bottom" title="Beterraba, Cenoura, Batata doce, Batata, Rabanete, Inhame, Aipim" style="cursor: pointer" class="fa fa-info-circle"></i></span><br><span>4 legumes&nbsp;<i data-toggle="tooltip" data-placement="bottom" title="Abobrinha, Berinjela, Brócolis, Pepino Comum, Tomate, Pimentão, Quiabo, Couve-flor" style="cursor: pointer" class="fa fa-info-circle"></i></span><br><span>2 temperos&nbsp;<i data-toggle="tooltip" data-placement="bottom" title="Alho poró, Salsa, Cebolinha, Coentro, Cebola, Cuminho em pó, Pimenta do Reino em pó, Coloral, Canela em pó, Canela em pó" style="cursor: pointer" class="fa fa-info-circle"></i></span><br><span>1 fruta&nbsp;<i data-toggle="tooltip" data-placement="bottom" title="Mamão, Laranja, Manga, Maracujá, Melancia, Jaca, Banana da terra, Banana prata, Tangerina, Lima, Limão" style="cursor: pointer" class="fa fa-info-circle"></i></span><br><span>1 produto Cosme e Damião&nbsp;<i data-toggle="tooltip" data-placement="bottom" title="Polpa de cupuaçu, Cupuaçu em pó, Cacau em pó, Amêndoa de cacau, Banana passa, Açaí, Mel de cacau, Geleia de cacau, Doce de cupuaçu, Tabletes sortidos de cacau amargo, cupuaçu amargo e cacau com banana, Polpa de acerola, Polpa de graviola" style="cursor: pointer" class="fa fa-info-circle"></i></span><br></p>
  <div class="flex">
     <div class="bottom">
        <a target="_blank" href="explanation.pdf" class="linkSaibaMais">Saiba mais!</a>
        <h4>R$ 290 / mês *</h4>
        <form target="_blank" action="https://pagseguro.uol.com.br/v2/pre-approvals/request.html" method="post"><input type="hidden" name="code" value="21E8C69B9F9F5D7FF489EF87AD452C9C"><input type="image" src="https://stc.pagseguro.uol.com.br/public/img/botoes/pagamentos/205x30-comprar.gif" name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!"></form>
    </div>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 text-center service-box">
  <h2 class="section-heading"><a data-toggle="tooltip" data-html="true" data-placement="bottom" title="&lt;img src=&quot;img/cestas/frutas.jpg&quot;&gt;" class="img-tooltip">Pomar Frutas</a></h2>
  <h4>5 itens</h4>
  <p><span>5 frutas&nbsp;<i data-toggle="tooltip" data-placement="bottom" title="Mamão, Laranja, Manga, Maracujá, Melancia, Jaca, Banana da terra, Banana prata, Tangerina, Lima, Limão" style="cursor: pointer" class="fa fa-info-circle"></i></span><br></p>
  <div class="flex">
     <div class="bottom">
        <a target="_blank" href="explanation.pdf" class="linkSaibaMais">Saiba mais!</a>
        <h4>R$ 120 / mês *</h4>
        <form target="_blank" action="https://pagseguro.uol.com.br/v2/pre-approvals/request.html" method="post"><input type="hidden" name="code" value="6EEBFB037F7FCF2AA4A52FBE0C0E00D5"><input type="image" src="https://stc.pagseguro.uol.com.br/public/img/botoes/pagamentos/205x30-comprar.gif" name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!"></form>
    </div>
</div>
</div>
</div>
<div class="row text-center">
   <p>* Preço pela assinatura mensal equivalente à entrega de cestas semanais, totalizando 4 cestas por mês.</p>
</div>
<div class="row text-center flex-horizontal-center">
   <div>
      <h2>Sentiu falta de algo? Precisa de mais itens?</h2>
      <h3><a href="#contato" class="page-scroll">Fale com a gente e nós faremos uma cesta especial para você!</a></h3>
  </div>
</div>
</div>