 <div class="row">
    <div class="col-lg-12 text-center">
       <h2 class="section-heading">Nossas feiras</h2>
       <hr class="primary">
   </div>
</div>
<div class="row flex-center">
    <div id="carousel" data-ride="carousel" class="carousel slide">
       <ol class="carousel-indicators">
          <li data-target="#carousel" data-slide-to="0"></li>
          <li data-target="#carousel" data-slide-to="1"></li>
          <li data-target="#carousel" data-slide-to="2"></li>
          <li data-target="#carousel" data-slide-to="3"></li>
          <li data-target="#carousel" data-slide-to="4"></li>
          <li data-target="#carousel" data-slide-to="5"></li>
          <li data-target="#carousel" data-slide-to="6"></li>
          <li data-target="#carousel" data-slide-to="7"></li>
          <li data-target="#carousel" data-slide-to="8"></li>
          <li data-target="#carousel" data-slide-to="9"></li>
          <li data-target="#carousel" data-slide-to="10"></li>
          <li data-target="#carousel" data-slide-to="11"></li>
      </ol>
      <div role="listbox" class="carousel-inner">
          <div class="item active">
             <img src="./img/feiras/greenville/1.jpg">
             <div class="carousel-caption">
                <p>greenville</p>
            </div>
        </div>
        <div class="item">
         <img src="./img/feiras/jaguaribe/1.jpg">
         <div class="carousel-caption">
            <p>jaguaribe</p>
        </div>
    </div>
    <div class="item">
     <img src="./img/feiras/jaguaribe/2.jpg">
     <div class="carousel-caption">
        <p>jaguaribe</p>
    </div>
</div>
<div class="item">
 <img src="./img/feiras/jaguaribe/3.jpg">
 <div class="carousel-caption">
    <p>jaguaribe</p>
</div>
</div>
<div class="item">
 <img src="./img/feiras/jaguaribe/4.jpg">
 <div class="carousel-caption">
    <p>jaguaribe</p>
</div>
</div>
<div class="item">
 <img src="./img/feiras/jaguaribe/5.jpg">
 <div class="carousel-caption">
    <p>jaguaribe</p>
</div>
</div>
<div class="item">
 <img src="./img/feiras/jaguaribe/6.jpg">
 <div class="carousel-caption">
    <p>jaguaribe</p>
</div>
</div>
<div class="item">
 <img src="./img/feiras/ufba/1.jpg">
 <div class="carousel-caption">
    <p>ufba</p>
</div>
</div>
<div class="item">
 <img src="./img/feiras/villatropical/1.jpg">
 <div class="carousel-caption">
    <p>villatropical</p>
</div>
</div>
<div class="item">
 <img src="./img/feiras/villatropical/2.jpg">
 <div class="carousel-caption">
    <p>villatropical</p>
</div>
</div>
<div class="item">
 <img src="./img/feiras/villatropical/3.jpg">
 <div class="carousel-caption">
    <p>villatropical</p>
</div>
</div>
<div class="item">
 <img src="./img/feiras/villatropical/4.jpg">
 <div class="carousel-caption">
    <p>villatropical</p>
</div>
</div>
</div>
<a href="#carousel" data-slide="prev" class="carousel-control left"></a><a href="#carousel" data-slide="next" class="carousel-control right"></a>
</div>
</div>