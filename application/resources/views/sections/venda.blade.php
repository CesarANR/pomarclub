 <div class="container">
    <div id="bairros" class="col-xs-12">
       <div class="row">
          <div class="col-lg-12 text-center">
             <h2 class="section-heading">Bairros de Atuação</h2>
             <hr class="primary">
         </div>
     </div>
     <div class="row flex-center">
      <div style="font-size: 17px;" class="container text-center">
         <div class="row">
            <div class="col-xs-12 col-sm-6">
               <h3>Sem taxa</h3>
               <p>Stella Mares, Itapuã, Piatã, Patamares, Boca do Rio, Imbuí, Paralela </p>
           </div>
           <div class="col-xs-12 col-sm-6">
               <h3>Com taxa</h3>
               <p>ACM, Pituba, Caminho das Arvores, Itaigara<br><small>R$10 / mês</small></p>
               <p> 
                  Rio Vermelho, Ondina, Barra, Federacao, Graca, Garcia<br><small>R$20 / mês</small>
              </p>
          </div>
      </div>
  </div>
</div>
</div>
</div>