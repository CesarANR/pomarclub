<div class="container">
   <h2 class="section-heading text-center">Quem somos</h2>
   <hr class="primary">
   <div class="row">
      <div class="col-sm-4 col-xs-4 text-center profile">
         <img src="img/quemsomos/matheus.jpg">
         <p>Matheus Passos</p>
         <b>
            <p style="font-size:.9em">Diretor Comercial (CCO)</p>
         </b>
      </div>
      <div class="col-sm-4 col-xs-4 text-center profile">
         <img src="img/quemsomos/aline.jpg">
         <p>Aline Nogueira</p>
         <b>
            <p style="font-size:.9em">Diretora Executiva (CEO)</p>
         </b>
      </div>
      <div class="col-sm-4 col-xs-4 text-center profile">
         <img src="img/quemsomos/giordana.jpg">
         <p>Ramakrisna de Jesus</p>
         <b>
            <p style="font-size:.9em">Diretor de Operações (COO)</p>
         </b>
      </div>
   </div>
</div>
<div id="trajetoria">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 text-center">
            <h2>Nossa trajetória</h2>
            <hr class="primary">
         </div>
      </div>
   </div>
   <div class="container">
      <div class="row justify flex-horizontal-center">
         <div class="col-xs-12 col-md-6 text-center text-muted">
            <p>A Pomar surgiu dando vida ao sonho de empreendedorismo sustentável de três amigos estudantes de Engenharia Ambiental. A ideia de construir uma plataforma de vendas de produtos orgânicos que pudesse propagar hábitos de consumo sustentável e respeito ao meio ambiente surgiu em 2014, durante um curso na University of Northampton, Inglaterra.</p>
            <div class="collapse">
               <p>Em 2015, a ideia foi selecionada para participar da pré-incubação de Startups da InovaPoli, dentro do ambiente acadêmico da Universidade Federal da Bahia. Diante da necessidade de aperfeiçoamento do plano de negócio, a equipe participou do curso "Como montar uma StartUp", do SEBRAE, através do qual desenvolveu o atual modelo de vendas da Pomar.</p>
               <h3>Nossa ideologia</h3>
               <hr class="primary">
               <p>A Pomar visa ajudar a promover o consumo sustentável. Acreditamos  que os consumidores podem se tornar agentes sociais engajados e atentos aos impactos de consumo.</p>
               <p>A Pomar visa ajudar a promover o consumo sustentável. Acreditamos que os consumidores podem se tornar agentes sociais engajados atentos aos impactos de consumo.</p>
               <p>Essa ideologia é refletida na nossa logística de funcionamento e entrega: levamos em consideração que a produção de orgânicos respeita os limites sazonais dos alimentos e que, por isso, os insumos produtivos são reduzidos.</p>
               <p>A Pomar acredita plenamente que agricultura orgânica é mais do que um modo produtivo, é uma visão de mundo e um movimento ativista. Junte-se a nós!</p>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-xs-12 text-center">
            <p><button data-toggle="collapse" data-target="#trajetoria .collapse">Saiba mais</button></p>
         </div>
      </div>
   </div>
</div>