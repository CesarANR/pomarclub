<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('includes.head')
</head>
<body id="page-top">
    @include('includes.menu')
    <header class="full-height">
     <div class="header-content">
        <div class="header-content-inner">
           <img src="img/logo-white.png" class="logo">
           <hr>
           <p><b>Temos a missão de levar à mesa das famílias soteropolitanas produtos orgânicos de qualidade de forma fácil e acessível. Alimentos da terra à sua mesa: um verdadeiro Pomar Urbano!</b></p>
           <div id="register-box" class="maybehide">
              <form id="register-form"><input id="name" type="text" placeholder="Nome" required=""><input id="email" type="email" placeholder="Email" required=""><br><br><button type="submit" class="btn btn-primary btn-xl page-scroll">Cadastre-se</button></form>
          </div>
          <div id="loggedin-box" class="maybehide">Obrigado pelo seu registro. Entraremos em contato em breve!<br><br><button id="unregister" class="btn btn-primary page-scroll">Sair</button>
          </div>
          <a href="#produtos" class="page-scroll cta">Confira nossas cestas</a>
      </div>
  </div>
</header>
<section id="servico">
    @include('sections.servicos')
</section>
<section id="produtos" class="bg-cestas">
    @include('sections.produtos')
</section>
<section id="venda">
    @include('sections.venda')
</section>
<section id="feiras" class="bg-feiras">
    @include('sections.feiras')
</section>
<section id="eventos" class="bg-primary">
    @include('sections.eventos')
</section>
<section id="portfolio" class="no-padding">
    @include('sections.portifolio')
</section>
<section id="quemsomos">
    @include('sections.quemsomos')
</section>
<div class="container">
 <div class="row">
    <h2 class="text-center">Nossos parceiros</h2>
    <hr class="primary">
    <div class="container">
       <div class="row flex-horizontal-center flex-wrap">
          <div class="parceiros col-xs-12 col-sm-4 flex-horizontal-center"><img src="img/parceiros/acpoba.jpg" class="max-img"></div>
          <div class="parceiros col-xs-12 col-sm-4 flex-horizontal-center"><img src="img/parceiros/agrossilvicultura.jpg" class="max-img"></div>
          <div class="parceiros col-xs-12 col-sm-4 flex-horizontal-center"><img src="img/parceiros/biofeira.jpg" class="max-img"></div>
      </div>
  </div>
</div>
</div>
<div id="contato">
 <div style="margin-bottom: .5em" class="text-center">
    <h2>Contato</h2>
    <hr class="primary">
    <p>Para maiores informações, entre em contato conosco!</p>
</div>
<div class="contatos">
    <div class="row">
       <div class="col-lg-3 col-lg-offset-3 text-center">
          <i class="fa fa-whatsapp fa-3x wow bounceIn"></i>
          <p class="no-margin"><a href="tel:+5571986923653">(71) 98692-3653</a></p>
          <p class="no-margin"> <a href="tel:+5571992856669">(71) 99285-6669</a></p>
      </div>
      <div class="col-lg-3 text-center">
          <i data-wow-delay=".1s" class="fa fa-envelope-o fa-3x wow bounceIn"></i>
          <p><a href="mailto:contato@pomar.club">contato@pomar.club</a></p>
      </div>
  </div>
  <div class="row">
   <div class="col-lg-3 col-lg-offset-3 text-center">
      <a target="_blank" href="//instagram.com/pomar.club">
         <i class="fa fa-instagram fa-3x wow bounceIn"></i><br>
         <p>@pomar.club</p>
     </a>
 </div>
 <div class="col-lg-3 text-center">
  <a target="_blank" href="https://www.facebook.com/pomar.club/?fref=ts">
     <i class="fa fa-facebook fa-3x wow bounceIn"></i><br>
     <p>facebook.com/pomar.club</p>
 </a>
</div>
</div>
</div>
</div>
</body>
</html>
